<?php
require_once "php/db.php";

DB::start();

$hash = $_GET['hash'];
if ($hash) {
    $st = DB::$db->prepare("SELECT recovery_hash FROM users WHERE recovery_hash=?;");
    $st->execute(array($hash));
    $res = $st->fetchColumn();
    if (!$res) {
        header("Location: ./index.php");
    }
} else {
    header("Location: ./index.php");
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Восстановление пароля.</title>
    <link rel="stylesheet" href="./css/bootstrap.min.css">
    <link rel="stylesheet" href="./css/style.css<?='?' . time()?>">
</head>
<body>
    <div class="container py-5">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-center mb-4">Восстановление пароля.</h2>
                    <div class="row">
                        <div class="col-md-6 mx-auto">
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="mb-0">Введите новый пароль.</h3>
                                </div>
                                <div class="card-body">
                                <form class="form" role="form" autocomplete="off" id="formChange" novalidate="" action="">
                                    <input type="hidden" id="hash" value="<?=$hash ?>">
                                    <div class="form-group">
                                        <input type="password" class="form-control form-control-lg rounded-1" placeholder="Пароль" name="forgot_pass1" id="forgot_pass1" required="" autocomplete="new-password" value="">
                                        <small class="forgot_pass-small-1">Пароль от 6 до 20 символов.</small>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control form-control-lg rounded-1" placeholder="Пароль" name="forgot_pass2" id="forgot_pass2" required="" autocomplete="new-password" value="">
                                        <small class="forgot_pass-small-2">Повторите пароль.</small>
                                    </div>
                                    <button type="button" class="btn btn-primary btn-lg float-right" id="btnChangePass">Готово</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    

    <script type="text/javascript" src="./js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="./js/popper.min.js"></script>
    <script type="text/javascript" src="./js/bootstrap.min.js"></script>
    <script type="text/javascript" src="./js/script.js<?='?' . time()?>"></script>

</body>
</html>