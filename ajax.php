<?php

require_once "php/db.php";
require_once "php/check_ajax.php";

DB::start();

$v1 = htmlspecialchars(stripslashes($_POST["v1"]));
$v2 = htmlspecialchars(stripslashes($_POST["v2"]));
$v3 = htmlspecialchars(stripslashes($_POST["v3"]));
$v4 = htmlspecialchars(stripslashes($_POST["v4"]));
$v5 = htmlspecialchars(stripslashes($_POST["v5"]));
$v6 = htmlspecialchars(stripslashes($_POST["v6"]));

switch ($v1) {
    case 'reg': // registration
	    echo registration_check($v2, $v3, $v4, $v5, $v6);
	    break;
    case 'login':
    	echo login_check($v2, $v3);
	    break;
	case 'forgot':
    	echo forgot_pass($v2);
	    break;
	case 'change':
    	echo change_pass($v2, $v3, $v4);
	    break;
}


