
/*Sending registration form*/

$("#btnRegGo").click(function(event) {
    $("#btnRegGo").prop("disabled", true);
    $.ajax({
        url: "ajax.php",
        type: "POST",
        data: {
            v1: "reg", 
            v2: $("#reg_email").val(),
            v3: $("#reg_fname").val(),
            v4: $("#reg_lname").val(),
            v5: $("#reg_pass1").val(),
            v6: $("#reg_pass2").val()
        },
        success: function(msg) {
            console.log(msg);

            $("#btnRegGo").prop("disabled", false);
            JSON.parse(msg).forEach(function(element, index) {
                switch (element) {
                    case 'ok': // все в порядке
                        $("#regModal").modal("hide");
                        $(".alert-msg").html("Регистрация прошла успешно.<br> Вы можете войти в личный кабинет.");
                        $("#alertModal").modal("show");
                        break;
                    case 'emailempty': // пустой email
                        $(".reg-email-small").html("Введите действительный e-mail.");
                        $(".reg-email-small").css("color", "red");
                        break;
                    case 'emailbig': // длинное поле email
                        $(".reg-email-small").html("Слишком длинное значение.");
                        $(".reg-email-small").css("color", "red");
                        break;
                    case 'wrongemail': // невалитный email
                        $(".reg-email-small").html("Формат адреса некорректен.");
                        $(".reg-email-small").css("color", "red");
                        break;
                    case 'emailexist': // email уже занят
                        $(".reg-email-small").html("Адрес уже занят.");
                        $(".reg-email-small").css("color", "red");
                        break;
                    case 'fnameempty': // пустое имя
                        $(".reg-fname-small").html("Введите имя.");
                        $(".reg-fname-small").css("color", "red");
                        break;
                    case 'fnamebig': // длинное имя
                        $(".reg-fname-small").html("Слишком длинное имя.");
                        $(".reg-fname-small").css("color", "red");
                        break;
                    case 'lnameempty': // пустая фамилия
                        $(".reg-lname-small").html("Введите фамилию.");
                        $(".reg-lname-small").css("color", "red");
                        break;
                    case 'lnamebig': // длинная фамилия
                        $(".reg-lname-small").html("Слишком длинная фамилия.");
                        $(".reg-lname-small").css("color", "red");
                        break;
                    case 'passshort': // короткий пароль
                        $(".reg-pass1-small").html("Пароль короткий.");
                        $(".reg-pass1-small").css("color", "red");
                        break;
                    case 'passbig': // длинный пароль
                        $(".reg-pass1-small").html("Слишком длинный пароль.");
                        $(".reg-pass1-small").css("color", "red");
                        break;
                    case 'passnotsame': // Пароли не совпадают
                        $(".reg-pass2-small").html("Пароли не совпадают.");
                        $(".reg-pass2-small").css("color", "red");
                        break;
                }
            });
        }
    });
});

/*Sending login form*/

$("#btnLogin").click(function(event){
    $("#btnLogin").prop("disabled", true);
    $.ajax({
        url: "ajax.php",
        type: "POST",
        data: {
            v1: "login", 
            v2: $("#email").val(),
            v3: $("#pass").val()
        },
        success: function(msg) {
            console.log(msg);

            $("#btnLogin").prop("disabled", false);
            JSON.parse(msg).forEach(function(element, index) {
                switch (element) {
                    case 'ok': // все в порядке
                        window.location.href = "user.php";
                        break;
                    case 'emailempty':
                        $(".email-small").html("Введите e-mail.");
                        $(".email-small").css("color", "red");
                        break;
                    case 'passempty':
                        $(".pass-small").html("Введите пароль.");
                        $(".pass-small").css("color", "red");
                        break;
                    case 'wrongpair':
                        $(".pass-small").html("Неверный пароль.");
                        $(".pass-small").css("color", "red");
                        break;
                }
            });
        }
    });
});

/*Sending forgot pass form*/

$("#btnForgotGo").click(function(event){
    $("#btnForgotGo").prop("disabled", true);
    $.ajax({
        url: "ajax.php",
        type: "POST",
        data: {
            v1: "forgot", 
            v2: $("#forgot_email").val()
        },
        success: function(msg) {
            console.log(msg);

            $("#btnForgotGo").prop("disabled", false);
            JSON.parse(msg).forEach(function(element, index) {
                switch (element) {
                    case 'ok': // все в порядке
                        $("#forgotModal").modal("hide");
                        $(".alert-msg").html("На e-mail, указанный при регистрации,<br> выслано письмо с инструкциями.");
                        $("#alertModal").modal("show");
                        break;
                }
            });
        }
    });
});

/*Sending change pass form*/

$("#btnChangePass").click(function(event){
    $("#btnChangePass").prop("disabled", true);
    $.ajax({
        url: "ajax.php",
        type: "POST",
        data: {
            v1: "change", 
            v2: $("#forgot_pass1").val(),
            v3: $("#forgot_pass2").val(),
            v4: $("#hash").val()
        },
        success: function(msg) {
            console.log(msg);

            $("#btnChangePass").prop("disabled", false);
            JSON.parse(msg).forEach(function(element, index) {
                switch (element) {
                    case 'ok': // все в порядке
                        window.location.href = "/index.php?forgot_pass=done";
                        break;
                    case 'pass1short': 
                        $(".forgot_pass-small-1").html("Пароль короткий.");
                        $(".forgot_pass-small-1").css("color", "red");
                        break;
                    case 'pass1big': 
                        $(".forgot_pass-small-1").html("Пароль длинный.");
                        $(".forgot_pass-small-1").css("color", "red");
                        break;
                    case 'passnotsame': 
                        $(".forgot_pass-small-2").html("Пароли не совпадают.");
                        $(".forgot_pass-small-2").css("color", "red");
                        break;    
                }
            });
        }
    });
});

/*Отправка формы при нажатии enter в любом поле формы*/
$(function(){
    $("#formLogin").keyup(function(event){
        if(event.keyCode == 13){
            event.preventDefault();
            $("#btnLogin").trigger("click");
        }
    });
    $("#formReg").keyup(function(event){
        if(event.keyCode == 13){
            event.preventDefault();
            $("#btnRegGo").trigger("click");
        }
    });
    $("#formForgot").submit(function(event){
        event.preventDefault();
    });
    $("#forgot_email").keyup(function(event){
        if(event.keyCode == 13){
            event.preventDefault();
            $("#btnForgotGo").trigger("click");
        }
    });
    $("#formChange").keyup(function(event){
        if(event.keyCode == 13){
            event.preventDefault();
            $("#btnChangePass").trigger("click");
        }
    });
});

/*Этот блок возвращает значение по умолчанию на поля ввода при наведении*/
$("#reg_email").focus(function(event){
    $(".reg-email-small").html("Введите действительный e-mail.");
    $(".reg-email-small").css("color","black");
});
$("#reg_fname").focus(function(event){
    $(".reg-fname-small").html("Введите полное имя.");
    $(".reg-fname-small").css("color","black");
});
$("#reg_lname").focus(function(event){
    $(".reg-lname-small").html("Введите полную фамилию.");
    $(".reg-lname-small").css("color","black");
});
$("#reg_pass1").focus(function(event){
    $(".reg-pass1-small").html("Пароль должен содержать от 6 до 20 символов.");
    $(".reg-pass1-small").css("color","black");
    $(".reg-pass2-small").html("&nbsp;");
    $(".reg-pass2-small").css("color","black");
});
$("#reg_pass2").focus(function(event){
    $(".reg-pass2-small").html("&nbsp;");
    $(".reg-pass2-small").css("color","black");
});
$("#email").focus(function(event){
    $(".email-small").html("E-mail введеный при регистрации.");
    $(".email-small").css("color","black");
});
$("#pass").focus(function(event){
    $(".pass-small").html("Пароль от 6 до 20 символов.");
    $(".pass-small").css("color","black");
});
$("#forgot_pass1").focus(function(event){
    $(".forgot_pass-small-1").html("Пароль от 6 до 20 символов.");
    $(".forgot_pass-small-1").css("color","black");
    $(".forgot_pass-small-2").html("Повторите пароль.");
    $(".forgot_pass-small-2").css("color","black");
});
$("#forgot_pass1").focus(function(event){
    $(".forgot_pass-small-2").html("Повторите пароль.");
    $(".forgot_pass-small-2").css("color","black");
});

