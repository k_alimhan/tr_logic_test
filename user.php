<?php
require_once "php/db.php";

DB::start();

$st = DB::$db->prepare("SELECT * FROM users WHERE session_hash=?;");
$st->execute(array($_COOKIE['session_id']));
$res = $st->fetchAll();

if (!$res) {
	header("Location: /index.php");
} else {
	$email = $res[0]['email'];
	$fname = $res[0]['fname'];
	$lname = $res[0]['lname'];
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Кабинет пользователя.</title>
    <link rel="stylesheet" href="./css/bootstrap.min.css">
    <link rel="stylesheet" href="./css/style.css<?='?' . time()?>">
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col text-center">
				Welcome, <strong><?=$fname . " " . $lname . "."?></strong>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="./js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="./js/popper.min.js"></script>
    <script type="text/javascript" src="./js/bootstrap.min.js"></script>
    <script type="text/javascript" src="./js/script.js<?='?' . time()?>"></script>
</body>
</html>