-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.5.61 - MySQL Community Server (GPL)
-- Операционная система:         Win64
-- HeidiSQL Версия:              9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Дамп структуры базы данных trtest
CREATE DATABASE IF NOT EXISTS `trtest` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `trtest`;

-- Дамп структуры для таблица trtest.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL DEFAULT '0',
  `fname` varchar(50) NOT NULL DEFAULT '0',
  `lname` varchar(50) NOT NULL DEFAULT '0',
  `pass_hash` varchar(32) NOT NULL DEFAULT '0',
  `session_hash` varchar(32) NOT NULL DEFAULT '0',
  `recovery_hash` varchar(32) NOT NULL DEFAULT '0',
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы trtest.users: ~8 rows (приблизительно)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `email`, `fname`, `lname`, `pass_hash`, `session_hash`, `recovery_hash`, `create_date`) VALUES
	(1, 'alim44@narod.ru', 'Алимхан', 'Кабардиев', '03549bc41ef454b36e7b295ba5a82023', '41a9b240a568bce5bbfd303805f9a5a1', 'c8a0cc51d57cd2b7af1d7a6e98ae9ca6', '2019-01-13 18:39:40'),
	(2, 'asas@2323.ru', 'asasas', 'asasa', 'e10adc3949ba59abbe56e057f20f883e', '9a1f7a28209322d6db97f88d71905be8', '23251f46d68bed5dcbcfc37342f05457', '2019-01-13 19:34:46'),
	(3, 'asas@2323.ru2', 'asasas', 'asasa', 'e10adc3949ba59abbe56e057f20f883e', 'b271e33bd9d552e8d362d3b5591cfb20', 'f82ad5dd87c8ee5369411d5320b402b2', '2019-01-13 20:13:26'),
	(4, 'alim444@narod.ru', 'asas', 'asasas', '96e79218965eb72c92a549dd5a330112', '786004059635a7e1d1a49b0fb44b9e6a', '0f0a09aff8805d71bf375d8338041179', '2019-01-13 21:22:59'),
	(5, 'as@as.as', 'as', 'as', '96e79218965eb72c92a549dd5a330112', 'f02399f7c5a5751d7e37fc0ae3bc511e', 'da1720913167e1f289091f2ab137e2fc', '2019-01-13 21:27:07'),
	(6, '5454@sdf.ru', '12312', '23234', '96e79218965eb72c92a549dd5a330112', '50f06d3dea7deafa4850f89018b7e73d', 'dde0f566617179aefff05398b2a683b3', '2019-01-13 21:30:41'),
	(7, '5454@sd2f.ru', '12312', '23234', '96e79218965eb72c92a549dd5a330112', 'b696e4e61430d81ae93b82695f4ac568', 'c71920abe824b4e6cae070201a73b3d5', '2019-01-13 21:34:41'),
	(8, 'sdas@ffh.ru', 'asdbaksdb', 'lsdnflsdnf', '96e79218965eb72c92a549dd5a330112', '7f4f79b3558ac4442d0df2e7358e0d18', 'afa24f129476cda2a21932d641ad0e52', '2019-01-13 21:35:15');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
