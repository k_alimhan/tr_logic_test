<?php
require_once "php/db.php";

DB::start();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Форма входа/регистрации. Тестовое задание.</title>
    <link rel="stylesheet" href="./css/bootstrap.min.css">
    <link rel="stylesheet" href="./css/style.css<?='?' . time()?>">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col d-flex form-wrapper">
                <div class="card login-card">
                    <div class="card-header">
                        <h3 class="mb-0">Личный кабинет.</h3>
                    </div>
                    <div class="card-body">
                        <form class="form" role="form" autocomplete="off" id="formLogin" novalidate="" action="">
                            <div class="form-group">
                                <input type="text" class="form-control form-control-lg rounded-1" name="email" id="email" required="" placeholder="E-mail" value="">
                                <small><span class="email-small">E-mail введеный при регистрации.</span></small>
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control form-control-lg rounded-1" placeholder="Пароль" name="pass" id="pass" required="" autocomplete="new-password" value="">
                                <small class="pass-small">Пароль от 6 до 20 символов.</small>
                            </div>
                            <div class="form-group text-right">
                                <a href="" class="forgot_password" data-toggle="modal" data-target="#forgotModal">Забыли пароль?</a>
                            </div>
                            <button type="button" class="btn btn-primary btn-lg float-right" id="btnLogin">Готово</button>
                            <button type="button" class="btn btn-success btn-lg float-left" id="btnReg" data-toggle="modal" data-target="#regModal">Регистрация</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Registration modal window -->

    <div class="modal fade" id="regModal" tabindex="-1" role="dialog" aria-lablelledby="regModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="regModalLable">Регистрация нового пользователя.</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-lable="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h5>Заполните все поля.</h5>


                    <form role="form" autocomplete="off" id="formReg" novalidate="" action="">
                        <div class="form-group">
                            <input type="text" class="form-control form-control-lg rounded-0" name="reg_email" id="reg_email" required="" placeholder="E-mail">
                            <small class="reg-email-small">Введите действительный e-mail.</small>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control form-control-lg rounded-0" name="reg_fname" id="reg_fname" required="" placeholder="Имя">
                            <small class="reg-fname-small">Введите полное имя.</small>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control form-control-lg rounded-0" name="reg_lname" id="reg_lname" required="" placeholder="Фамилия">
                            <small class="reg-lname-small">Введите полную фамилию.</small>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control form-control-lg rounded-0" id="reg_pass1" required="" autocomplete="new-password"  placeholder="Пароль.">
                            <small class="reg-pass1-small">Пароль должен содержать от 6 до 20 символов.</small>                  
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control form-control-lg rounded-0" id="reg_pass2" required="" autocomplete="new-password"  placeholder="Повторите пароль.">
                            <small class="reg-pass2-small">&nbsp;</small>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary float-left" data-dismiss="modal">Закрыть</button>
                    <button class="btn btn-primary float-right" id="btnRegGo">Готово</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Registration modal window END -->

    <!-- Forgot password modal window -->

    <div class="modal fade" id="forgotModal" tabindex="-1" role="dialog" aria-lablelledby="forgotModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="regModalLable">Восстановление пароля.</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-lable="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h5>Введите адрес электронной почты.</h5>
                    <form class="form" role="form" autocomplete="off" id="formForgot" novalidate="" action="">
                        <div class="form-group">
                            <input type="text" class="form-control form-control-lg rounded-0" name="forgot_email" id="forgot_email" required="" placeholder="Адрес электронной почты.">
                            <small class="forgot-email-small">Введите действительный адрес.</small>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary float-left" data-dismiss="modal">Закрыть</button>
                    <button class="btn btn-primary float-right" data-dismiss="modal" id="btnForgotGo">Готово</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Forgot password modal window END -->

    <!-- Info modal window -->
    
    <div class="modal fade" id="alertModal" tabindex="-1" role="dialog" aria-lablelledby="alertModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="alert-msg"></div>
                </div>
                <div class="modal-footer justify-content-center">
                    <button class="btn btn-primary float-right" data-dismiss="modal" id="btnAlertDone">Готово</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Info modal window -->

    <script type="text/javascript" src="./js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="./js/popper.min.js"></script>
    <script type="text/javascript" src="./js/bootstrap.min.js"></script>
    <script type="text/javascript" src="./js/script.js<?='?' . time()?>"></script>
    <script type="text/javascript">
        /*Выводит alert при успешной смене пароля*/
        $(function(){
            if ('<?=$_GET['forgot_pass'] ?>' == 'done') {
                $(".alert-msg").html("Ваш пароль изменен.");
                $("#alertModal").modal("show");
            }
        });
    </script>
</body>
</html>