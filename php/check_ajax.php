<?php
/*
* Функция проверяет данные введенные при регистрации
*
* @var $v2 - email
* @var $v3 - fname
* @var $v4 - lname
* @var $v5 - pass1
* @var $v6 - pass2
*
* @return json строка содержащая коды ошибок или 'ok' если регистрация успешна
*/
function registration_check($v2, $v3, $v4, $v5, $v6)
{
	$result = array();

    //  проверка поля E-mail

	if (strlen($v2) == 0) {
        $result[] = 'emailempty'; // пустая строка e-mail
    } elseif (strlen($v2) > 50) {
        $result[] = 'emailbig'; // большая строка e-mail
    } elseif (!filter_var($v2, FILTER_VALIDATE_EMAIL)) {
    	$result[] = 'wrongemail';
    } elseif (DB::email_exist($v2)) {
    	$result[] = 'emailexist';
    }
    //  проверка поля Имя
    if (strlen($v3) == 0) {
        $result[] = 'fnameempty'; // пустая строка fname
    } elseif (strlen($v3) > 50) {
        $result[] = 'fnamebig'; // большая строка fname
    }
    //  проверка поля Фамилия
    if (strlen($v4) == 0) {
        $result[] = 'lnameempty'; // пустая строка lname
    } elseif (strlen($v4) > 50) {
        $result[] = 'lnamebig'; // большая строка lname
    }
    //  проверка поля Пароль1
    if (strlen($v5) < 6) {
        $result[] = 'passshort'; // пустая строка pass1
    } elseif (strlen($v5) > 20) {
        $result[] = 'passbig'; // большая строка pass2
    }
    // проверка совпатедения паролей
    if ($v5 != $v6) {
        $result[] = 'passnotsame';  //  пароли не совпадают
    }

    if (count($result) != 0) {
    	return json_encode($result);
    } else {
    	$result[] = 'ok';
    	DB::registrate($v2, $v3, $v4, md5($v5));
    	return json_encode($result);
    }

}


/*
* Функция проверяет данные введенные при входе
*
* @var $v2 - email
* @var $v3 - pass
*
* @return json строка содержащая коды ошибок или 'ok' если вход успешен
*/
function login_check($v2, $v3)
{
    $result = array();
    $session_hash = DB::check_login_pass($v2, $v3);
    //  проверка поля E-mail
    if (strlen($v2) == 0) {
        $result[] = 'emailempty'; // пустая строка e-mail
    }

    //  проверка поля Пароль
    if (strlen($v3) == 0) {
        $result[] = 'passempty'; // пустая строка pass
    }

    // проверка пары email pass
    if (!$session_hash) {
        $result[] = 'wrongpair';
    }

    if (count($result) == 0) {
        $result[] = 'ok';
        setcookie("session_id",$session_hash,time()+3600*24*365);
        return json_encode($result);
    } else {
        return json_encode($result);
    }
}

/*
* Функция проверяет данные введенные при смене пароля
*
* @var $v2 - pass1
* @var $v3 - pass2
* @var $v4 - recovery_hash
*
* @return json строка содержащая коды ошибок или 'ok' если вход успешен
*/
function change_pass($v2, $v3, $v4)
{
    $result = array();

    if (strlen($v2) < 6) {
        $result[] = 'pass1short'; // пустая строка pass1
    } elseif (strlen($v2) > 20) {
        $result[] = 'pass1big'; // большая строка pass2
    }

    if ($v2 != $v3) {
        $result[] = 'passnotsame';  //  пароли не совпадают
    }

    


    if (count($result) == 0) {
        $result[] = 'ok';
        DB::change_pass($v2, $v4);
        return json_encode($result);
    } else {
        return json_encode($result);
    }
}
/*
* Функция отправляет ссылку для восстановления пароля по почте
*
* @var $email - email =)
*
* @return json строка содержащая коды ошибок и отладочную информацию и 'ok', если вход успешен.
*/
function forgot_pass($email)
{
    $result = array();
    $result[] = 'ok';
    $recovery_hash = DB::get_recovery_hash($email);
    if (!$recovery_hash) {
        return json_encode($result);
    }
    
    require_once 'PHPMailer/PHPMailerAutoload.php';
    $mail = new PHPMailer();
    $mail->CharSet = 'UTF-8';
    $mail->isSMTP();
    $mail->SMTPAuth = true;
    $mail->SMTPDebug = 0;
    $mail->Host = "ssl://smtp.mail.ru";
    $mail->Port = 465;
    $mail->Username = "trtest@avihelper.ru";
    $mail->Password = "zxcv1234";
    $mail->setFrom('trtest@avihelper.ru', '');        
    $mail->addAddress($email, '');
    $mail->Subject = "Восстановление пароля.";
    $body = "Пройдите по ссылке для восстановления пароля. <br>" . "<a href='http://avihelper.ru/trtest/recovery.php?hash=$recovery_hash' target='_blank'>http://avihelper.ru/trtest/recovery.php?hash=$recovery_hash</a>";
    $result[] = $email;
    $result[] = $body;
    $mail->msgHTML($body);
    $result[] = $mail->send();
    $result[] = $mail->ErrorInfo;
    return json_encode($result);
}