<?php
/*
*   Author: Alimkhan Kabardiev <alim44@yandex.ru>
*   
*   Connection to database using PHP::PDO
*
*/

class DB
{
    private static $user = 'trtest'; // имя пользователя
    private static $password = 'trtest12'; // пароль
    private static $dsn = 'mysql:host=' . 'localhost' . ';dbname=' . 'trtest' . ';charset=utf8';
    private static $opt = [
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES   => false,
    ];
    public static $db;

    // init db connection and put handle to $db
    public static function start()
    {
        try {
            DB::$db = new PDO(DB::$dsn, DB::$user, DB::$password, DB::$opt);
        } catch(PDOException $e) {
            echo "You have an error: " . $e->getMessage() . "<br>";
            echo "On line: " . $e->getLine();
        }
    }

    // register new user
    public static function registrate($email, $fname, $lname, $pass_hash)
    {
        $st = DB::$db->prepare("INSERT INTO users(
            email, 
            fname, 
            lname, 
            pass_hash, 
            session_hash, 
            recovery_hash
        ) VALUES(?, ?, ?, ?, ?, ?);");
        $st->execute(array($email, $fname, $lname, $pass_hash, md5(microtime(true) . $email), md5(microtime(true) . $fname)));
        
    }
    // checking email 
    public static function email_exist($email)
    {
        $st = DB::$db->prepare("SELECT email FROM users WHERE email=?");
        $st->execute(array($email));
        $res = $st->fetchColumn();
        if ($res) {
            return true;
        } else {
            return false;
        }
    }
    // checking login and pass pair
    public static function check_login_pass($email, $pass)
    {
        $st = DB::$db->prepare("SELECT email FROM users WHERE email=? AND pass_hash=?");
        $st->execute(array($email, md5($pass)));
        $res = $st->fetchColumn();
        if ($res) {
            $hash = md5(microtime(true) . $email);
            $st = DB::$db->prepare("UPDATE users SET session_hash=? WHERE email=?");
            $st->execute(array($hash, $email));
            setcookie("session_id", $hash, time()+3600*24*365);
            return $hash;
        } else {
            return false;
        }
    }
    // return and set new recovery hash
    public static function get_recovery_hash($email)
    {
        $st = DB::$db->prepare("SELECT email FROM users WHERE email=?");
        $st->execute(array($email));
        $res = $st->fetchColumn();
        if ($res) {
            $hash = md5(microtime(true) . $email);
            $st = DB::$db->prepare("UPDATE users SET recovery_hash=? WHERE email=?");
            $st->execute(array($hash, $email));
            return $hash;
        } else {
            return false;
        }
        
    }

    // change a password in db
    public static function change_pass($v2, $v4)
    {
        $st = DB::$db->prepare("UPDATE users SET pass_hash=?, session_hash=?, recovery_hash=? WHERE recovery_hash=?");
        $st->execute(array(md5($v2),"", "", $v4));
    }

}
?>